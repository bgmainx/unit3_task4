package Entities;

/**
 * @author Borimir Georgiev
 */
public class Contact
{
    private String name;
    private String phoneNumber;

    public Contact(String name, String phoneNumber)
    {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName()
    {
        return this.name;
    }

    @Override
    public String toString()
    {
        return  "Name: " + this.name  + "\n" +
                "Number: " + this.phoneNumber + "\n" ;
    }
}
