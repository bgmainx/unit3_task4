package Entities;

import Interfaces.ContactsManager;

import java.util.HashMap;

/**
 *
 * @author Borimir Georgiev
 */
public class PhoneBook implements ContactsManager
{
    HashMap<String, Contact> contacts;

    public PhoneBook()
    {
        this.contacts = new HashMap<>();
    }

    @Override
    public int add(Contact contact)
    {
        if (!contacts.containsKey(contact.getName()))
        {
            contacts.put(contact.getName(), contact);
            return 1;
        }

        return 0;
    }

    @Override
    public int remove(String name)
    {
        if (contacts.containsKey(name))
        {
            contacts.remove(name);
            return 1;
        }

        return 0;
    }

    @Override
    public Contact lookUp(String name)
    {
        if (!contacts.containsKey(name))
        {
            System.out.println("A contact with the given name doesn't exist.");
            return null;
        }

        return contacts.get(name);
    }

    @Override
    public void listAll()
    {
        if (!contacts.isEmpty())
        {
            contacts.forEach((k, v) ->
                    System.out.println(v));
        } else
        {
            System.out.println("Your phone book is empty.");
        }
    }
}
