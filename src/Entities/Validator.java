package Entities;

/**
 * @author Borimir Georgiev
 */
public abstract class Validator
{
    public static int validateName(String name)
    {
        // name should contain only letters
        if (name.equals(" ") || !name.matches("[A-Z][a-zA-Z][^#&<>\\\"~;$^%{}*?\\d]{1,20}$"))
        {
            return 0;
        }

        return 1;
    }

    public static int validateNumber(String number)
    {
        // valid number pattern: 0884321353
        if (number.equals(" ") || !number.matches("^(([0-9]{4}|0)[0-9]{6})$"))
        {
            return 0;
        }

        return 1;
    }
}
