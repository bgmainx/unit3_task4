package Entities;


import java.util.Scanner;

/**
 * @author Borimir Georgiev
 */
public class Menu
{
    private static final String MSGERR = "Please input a correct name..";
    private PhoneBook phoneBook;
    private Scanner sc;

    public Menu()
    {
        this.phoneBook = new PhoneBook();
        this.sc = new Scanner(System.in);
    }

    /**
     *  Prints menu, reads user choice and acts accordingly.
     */
    public void printMenu()
    {
        System.out.println("Please choose an option:");
        System.out.println("[1] Add contact");
        System.out.println("[2] Remove contact");
        System.out.println("[3] Look up contact");
        System.out.println("[4] List all contacts");

        int userChoice = read();
        proceed(userChoice);
    }

    public int read()
    {
        int userChoice = 0;

        // Checks if it's a number.
        try
        {
            userChoice = Integer.parseInt(sc.nextLine());
        } catch (NumberFormatException e)
        {
            System.out.println("Invalid choice! Input must be a number.");
            printMenu();
            read();
        }

        // Checks if the list of options contains a number same as the users num.
        if (userChoice < 5)
        {
            return userChoice;

        } else
        {
            System.out.println("There isn't a choice with such number.");
            System.out.println("Please input a number between 1 and 3");
            printMenu();
            return read();
        }
    }

    public void proceed(int userChoice)
    {
        switch (userChoice)
        {
            case 1:
                addOption();
                break;
            case 2:
                removeOption();
                break;
            case 3:
                lookUpOption();
                break;
            case 4:
                phoneBook.listAll();
                printMenu();
                break;
        }
    }

    private void lookUpOption()
    {
        System.out.println("Please input contact name: ");
        String name = sc.nextLine();

        if (Validator.validateName(name) == 0)
        {
            System.out.println(MSGERR);
            name = sc.nextLine();
        }

        Contact contact = phoneBook.lookUp(name);

        if (contact != null)
        {
            System.out.println(contact.toString());
        } else
        {
            lookUpOption();
        }

        printMenu();
    }

    private void removeOption()
    {
        System.out.println("Please input contact name to be removed: ");
        String name = sc.nextLine();

        if (Validator.validateName(name) == 0)
        {
            System.out.println(MSGERR);
            name = sc.nextLine();
        }

        if (phoneBook.remove(name) == 0)
        {
            System.out.println("A contact with the given name doesn't exist.");
            printMenu();
        }

        printMenu();
    }

    /**
     * Awaits new contact info, validates and adds it to the phone book.
     */
    private void addOption()
    {
        System.out.println("Please input contacts name and phone number..");
        System.out.println("Contact name: ");
        String name = sc.nextLine();

        if (Validator.validateName(name) == 0)
        {
            System.out.println("Please input a correct name..");
            name = sc.nextLine();
        }

        System.out.println("Contact number: ");

        String number = sc.nextLine();
        if (Validator.validateNumber(number) == 0)
        {
            System.out.println("Please input a real phone number");
            number = sc.nextLine();
        }

        Contact contact = new Contact(name, number);

        if (phoneBook.add(contact) == 0)
        {
            System.out.println("This contact exists already!");
            printMenu();
        }

        printMenu();
    }

}
