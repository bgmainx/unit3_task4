package Interfaces;

import Entities.Contact;

/**
 * Simple phone contacts manager. Both add, remove methods return '1' for successful operation
 * and '0' for unsuccessful one.
 *
 * @author Borimir Georgiev
 */
public interface ContactsManager
{
    public int add(Contact contact);

    public int remove(String name);

    public Contact lookUp(String name);

    public void listAll();
}
