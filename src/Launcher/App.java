package Launcher;

import Entities.Menu;

/**
 * @author Borimir Georgiev
 */
public class App
{
    public static void run()
    {
        Menu menu = new Menu();
        menu.printMenu();
    }
}
